<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.03.2021
 * Time: 18:56
 */
class Console extends BaseClass
{

   public $name;

   public $image;

   public $price;

   public $reviews;

   public $review_id;

   public $user_id;

    public static function getTableName()
    {
        return 'consoles';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function getPrice(){

        return ($this->price);
    }
    public function getReview()
    {
        return Reviews::find($this->review_id);
    }
    public function getUser()
    {
        return Users::find($this->user_id);
    }

    public function getAssembly(){

        $assemblyConsole = Assembly::find(10);
        $assemblyConsole->parent_console_id = $this->getId();

        return $assemblyConsole;
    }
}
