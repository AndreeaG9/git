<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.2021
 * Time: 14:33
 */
class Assembly extends Console
{
    public $percent;

    public $parent_console_id;

    public function getPrice()
    {
        $parentPrice = Console::find($this->parent_console_id)->getId();

        return $parentPrice * $this->percent / 100;
    }
}